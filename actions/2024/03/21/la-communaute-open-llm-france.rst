.. index::
   pair: Michel Marie Maudet ; OpenLLM-France (2024-03-21)

.. _maudet_2024_03_21:

==========================================================================
2024-03-21 **La communauté OpenLLM France** par Michel Marie Maudet
==========================================================================

- :ref:`alposs_2024:maudet_2024_03_21`
