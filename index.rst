.. index::
   pair: Large Language Models; OpenLLM France
   ! OpenLLM France


|FluxWeb| `RSS <https://gdevops.frama.io/ia/openllm-france/rss.xml>`_

.. _openllm_france:

=============================================================
**Open LLMFrance**
=============================================================

- https://github.com/mmaudet
- https://www.openllm-france.fr/
- https://github.com/OpenLLM-Europe
- https://github.com/OpenLLM-France
- https://github.com/OpenLLM-France/Manifesto

.. toctree::
   :maxdepth: 6

   actions/actions
